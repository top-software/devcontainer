ARG NITRILE_VERSION
FROM docker.io/cleanlang/nitrile:$NITRILE_VERSION

RUN \
	apt-get update -qq && \
	apt-get upgrade -qq && \
	apt-get install -qq --no-install-recommends \
		build-essential ca-certificates cmake curl firefox-esr git git-lfs libssl-dev lld mkdocs ncat \
		netcat-openbsd procps psmisc p7zip-full time unzip wget zsh && \
	apt-get autoremove -qq && apt-get autoclean -qq
RUN git lfs install

# Install oh-my-zsh with prompts removed.
RUN sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh) --unattended"

# Install oh-my-zsh autosuggestion plugin (needs to be done in a different way than other plugins
# As it is developed by users.). Same for syntax-highlighting. The debian packages
# did not work.
RUN git clone https://github.com/zsh-users/zsh-autosuggestions ~/.oh-my-zsh/plugins/zsh-autosuggestions
RUN git clone https://github.com/zsh-users/zsh-syntax-highlighting ~/.oh-my-zsh/plugins/zsh-syntax-highlighting
RUN git clone https://github.com/tymm/zsh-directory-history ~/.oh.my-zsh/plugins/zsh-directory-history

# Copy the preprepared zsh file to the zshrc file of the container.
COPY .zshrc /root/.zshrc

ARG NODE_VERSION
RUN curl https://nodejs.org/dist/v"$NODE_VERSION"/node-v"$NODE_VERSION"-linux-x64.tar.xz | tar xJ -C /usr --strip-components=1

# Required to update dependencies
RUN npm install -g npm-check-updates

COPY onStartup.sh /onStartup.sh
CMD /onStartup.sh
