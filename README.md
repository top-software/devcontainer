# devcontainer

This extends the [Nitrile](https://gitlab.com/clean-and-itasks/nitrile/)
[docker images](https://hub.docker.com/r/cleanlang/nitrile) with tools and libraries commonly used for developing Clean
projects. The image can for instance be used as a
[devcontainer within VS Code](https://code.visualstudio.com/docs/devcontainers/containers).

The resulting images are [published on the Docker Hub registry](https://hub.docker.com/r/cleanlang/devcontainer) as
`cleanlang/devcontainer:VERSION`.

## Versioning

The versioning scheme is `x.y.z-a`, where `x.y.z` is the version of the Nitrile image the devcontainer image is based
on.  `a` starts at `0` for each new Nitrile version and is incremented in case other included software is updated, e.g.
[Node.js](https://nodejs.org/en).

Commit tags of the format (`x.y.z-a` without a `v` prefix) are used to indicate releases, which are automatically
pushed to the registry in the pipeline.

## License

The package is licensed under the BSD-2-Clause license; for details, see the
[LICENSE](/LICENSE) file.
