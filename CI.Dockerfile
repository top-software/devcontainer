ARG CI_COMMIT_TAG
FROM docker.io/cleanlang/devcontainer:$CI_COMMIT_TAG

RUN printf "clm:\n  parallel_jobs: 1" > ~/.nitrile/nitrile-settings.yml
